import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
import {Router, Route, IndexRoute, hashHistory} from "react-router";
import App from "./pages/App";
import Home from "./pages/Home";
import Finder from "./pages/Finder";
import  "../styles/style.scss"

const app = document.getElementById("content");

ReactDOM.render(<Router history={hashHistory}>
					<Route path="/" component={App}>
						<IndexRoute component={Home}/>
						<Route path="finder" component={Finder} />
					</Route>
				</Router>, 	app);


/*
The view of this app is created with React Single Page App style

Our single page is defined in App(Layout), all the children are other pages that the App(Layout) will render if the path match, for instance:

<Route path="finder" component={Finder} />

If the path is http://localhost:8080/#finder App(Layout) will render Finder

*/