
import React from "react";
import MenuOption from "./MenuOption";

export default class Menu extends React.Component{

	constructor(){
		super();
	}

	componentWillMount(){
		this.setState({
				stateHome : "active",
				stateFinder : ""
			});
	}

	handleClick(option){
		this.setState({
			stateHome : "",
			stateFinder : ""
		});
		if(option === "Home"){
			this.setState({
				stateHome : "active"
			});
		}else{
			this.setState({
				stateFinder : "active"
			});
		}
	}

	render(){
		return (<nav>
					<ul className="nav nav-pills pull-right">

						<MenuOption classOption={this.state.stateHome}
									text="Home"
									path="/"
									onClick={()=>this.handleClick("Home")}
									>
						</MenuOption>
						<MenuOption classOption={this.state.stateFinder}
									text="Finder"
									path="/finder"
									onClick={()=>this.handleClick("Finder")}
									>
						</MenuOption>

			        </ul>
			        <h3 className="text-muted">Repository Finder</h3>
				</nav>)

	}

}


/*
Note:

You can use this way to avoid implement onClick on MenuOption
This is the correct way to call a method with a parameter.
<MenuOption classOption={this.state.stateHome}
									text="Home"
									path="/"
									onClick={this.handleClick.bind(this,"Home")}
									>

*/
