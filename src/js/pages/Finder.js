import React from "react";
import FormularioBusqueda from "./FormularioBusqueda";
import Resultados from "./Resultados";
import * as FinderActions from "../actions/FinderActions.js";
import FinderStore from "../store/FinderStore";

export default class Finder  extends React.Component {
  //El estado permite darle dinamismo a la aplicación
  //se debe ver que cambia, los campos de entrada y los resultados de salida
  constructor(){
    super();
    this.getRepos = this.getRepos.bind(this);
    this.state = {
      resultados : [],
      usuario : 'jerviver21',
      incluirMiembro: false
    }
  }

  //Es un hook, que se ejecuta al momento de iniciar.
  componentDidMount() {
    FinderActions.findRepos(this.state);
  }

  componentWillMount(){
    FinderStore.on("change", this.getRepos);
  }

  componentWillUnmount(){
    FinderStore.removeListener("change", this.getRepos);
  }

  getRepos(){
    this.setState({
      resultados: FinderStore.getRepos()
    });
  }

  //Funciona como un callback, este componente lo pasa como un props al formulario y cuando el form cambia de estado lo llama.
  cambiarCriterioBusqueda(estado){
    this.setState(estado);
    FinderActions.findRepos(estado);
  }

  render(){
    return <div className="panel panel-default">
        <div className="panel-body">
            <FormularioBusqueda  usuario={this.state.usuario}
                                                incluirMiembro={this.state.incluirMiembro}
                                                onBuscar={this.cambiarCriterioBusqueda.bind(this)}/>
            <Resultados resultados={this.state.resultados}/>
        </div>
    </div>;
  }
}