import {Link} from "react-router" ;
import React from "react";

export default class MenuOption extends React.Component{

	render(){
		return (<li className={this.props.classOption} onClick={this.props.onClick}>
			       <Link to={this.props.path}>{this.props.text}</Link>
			   </li>)
			          
	}

}