import React from "react";

export default class FormularioBusqueda extends React.Component{
  //Algunos de los props se convierte en el estado de este componente.
  constructor(props){
    super();
    this.state = {
      usuario : props.usuario,
      incluirMiembro : props.incluirMiembro
    }
  }

  //Los handles permiten manejar eventos de submit, change, etc
  handleUsuario(ev){
      this.setState({usuario : ev.target.value});
  }

  handleIncluirMiembro(ev){
      this.setState({incluirMiembro : ev.target.checked});
  }

  handleSubmit(ev){
      ev.preventDefault();
      this.props.onBuscar({
        usuario : this.state.usuario,
        incluirMiembro: this.state.incluirMiembro
      })
  }

  render(){
    return <form className="form-horizontal" onSubmit={this.handleSubmit.bind(this)}>
      <div className="form-group">
        <div className="col-lg-9">
          <input  className="form-control" type="text"
                      value={this.state.usuario}
                      onChange={this.handleUsuario.bind(this)}/>
        </div>
        <div  className="col-lg-3">
          <button className="btn btn-default" type="submit">Buscar</button>
        </div>
      </div>
      <div className="form-group">
        <div className="col-lg-10">
          <div className="checkbox">
            <label>
              <input type="checkbox"
                          checked={this.state.incluirMiembro}
                          onChange={this.handleIncluirMiembro.bind(this)}/>
              &nbsp;Incluir repositorios donde el usuario es miembro
            </label>
          </div>
        </div>
      </div>
      <hr/>
    </form>
    ;
  }
}