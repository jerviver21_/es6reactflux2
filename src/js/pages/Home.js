import React from "react";

export default class Home extends React.Component{

	render(){
		return(
			<div className="contentf">
				<h2>React Examples</h2>
				<p>This application show us a way to work with React JS, using React, React router, Bootstrap, Browserify, Gulp, Flux and other tools that allow us to increase the performance in delivering front-end applications.</p>
			</div>
		)
	}

}