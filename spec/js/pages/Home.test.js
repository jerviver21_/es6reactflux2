import  React from 'react';
import  Home from '../../../src/js/pages/Home';
import { mount } from 'enzyme';

beforeEach(() => {

});

afterEach(() => {

});

test('Home render the text of Home Component', () => {
  const wrapper = mount(
    <Home />
  );
  const h2 = wrapper.find('h2');
  expect(h2.text()).toBe("React Examples");
});
